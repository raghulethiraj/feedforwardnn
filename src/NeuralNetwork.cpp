#include "NeuralNetwork.hpp"
#include "Perceptron.hpp"

#include <cmath>
#include <iostream>

using namespace std;

NeuralNetwork::NeuralNetwork(const vector<int> layerNodes) {

  this->layerNodes = layerNodes;

  initializeLayers();
}

void NeuralNetwork::initializeLayers() {

  for (int i = 0; i < layerNodes.size(); i++) {

    // Fetches num of nodes for each layer
    int numNodes = layerNodes[i];

    vector<Perceptron> layerPerceptrons;
    
    for (int j = 0; j < numNodes; j++) {
      // If input layer: set node input weight to 1 and bias to 0
      if( i == 0 ) {
	Perceptron node({1.0}, 0.0);

	// An input node just returns the input as output
	// to other nodes
	node.setActivationFunction([](double value) {
	    return value;
	});
	
	layerPerceptrons.push_back(node);
 
      } else if ( i == layerNodes.size() - 1 ) {
	Perceptron node(layerNodes[i - 1]);
	
	// If output layer: set activation function to return weight sums
	node.setActivationFunction([](double value) {
	    return value;
	});
	
	layerPerceptrons.push_back(node);
 
      } else {
	// A node is connected to all the nodes in prev layer
	Perceptron node(layerNodes[i - 1]);

	// If hidden node: set activation function to tanh
	node.setActivationFunction([](double value) {
	    if ( value < -20.)
	      return -1.;
	    else if ( value > 20.)
	      return 1.;
	    else
	      return tanh(value);      
	});
	
	layerPerceptrons.push_back(node);
      }
    }
    brain.push_back(layerPerceptrons);
  }
}

vector<double> NeuralNetwork::predict(vector<double> _inputs) {

  /*
    1. Feed inputs into input layer
    2. Compute outputs in each layer
    3. Apply softmax on output layer
   */

  for (int i = 0; i < brain.size(); i++) {

    int numNodes = layerNodes[i];

    for (int j = 0; j < numNodes; j++) {

      // If input layer: inject inputs
      if( i == 0 ) {
	brain[i][j].computeOutput({_inputs[j]});
	//cout << "[Layer][Node] : [" << i << "][" << j << "]" << endl;
	//cout << "Computed Output is : " << brain[i][j].getOutput() << endl;
      } else {
	brain[i][j].computeOutput(generateVector(brain[i - 1]));
       
	//cout << "[Layer][Node] : [" << i << "][" << j << "]" << endl;
	//cout << "Given Input :" << endl;

	//for (int k = 0; k < inputs.size(); k++) {
	  //cout << inputs[k] << "\t";
	  //}
	//cout << endl;

	//cout << "Computed Output is : " << brain[i][j].getOutput() << endl;
      }
    }
  }

  // Apply softmax here to output layer
  vector<double> output = softmax(brain[brain.size() - 1]);

  return output;
}

vector<double> NeuralNetwork::generateVector(vector<Perceptron> layerPerceptrons) {

  vector<double> layerOutputs;

  for (int i = 0; i < layerPerceptrons.size(); i++) {
    layerOutputs.push_back(layerPerceptrons[i].getOutput());
  }

  return layerOutputs;
}

vector<double> NeuralNetwork::softmax(vector<Perceptron> perceptrons) {

  double max = perceptrons[0].getOutput();

  // Find the max value to compute scale factor
  for (int i = 1; i < perceptrons.size(); i++) {
    if(perceptrons[i].getOutput() > max)
      max = perceptrons[i].getOutput();
  }

  double scale = 0.;

  for (int i = 0; i < perceptrons.size(); i++) {
    scale += exp(perceptrons[i].getOutput() - max); 
  }

  vector<double> output;

  for (int i = 0; i < perceptrons.size(); i++) {
    output.push_back(exp(perceptrons[i].getOutput() - max)/scale);
  }

  return output;
}
