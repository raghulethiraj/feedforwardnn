#include <iostream>
#include <vector>
#include "NeuralNetwork.hpp"

using namespace std;

int main(int argc, char *argv[]) {

  NeuralNetwork nn({2,4,3,2});

  vector<double> output = nn.predict({100, 10});

  cout << "Computed outputs" << endl;
  for (int i = 0; i < output.size(); i++) {
    cout << output[i] << endl;
  }

  return 0;
}
