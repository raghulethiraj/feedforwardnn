#pragma once

#include <vector>
#include <functional>
#include "Perceptron.hpp"

class NeuralNetwork
{
public:
  // (No.Inputs, No.Outputs, No.HiddenLayers, vector of nodes in each hidden layer)
  NeuralNetwork(const std::vector<int>);

  // A vector double of inputs to predict from
  std::vector<double> predict(std::vector<double>);
private:
  // Stores the num of nodes for each layer
  std::vector<int> layerNodes;

  // A network of all the perceptrons
  std::vector<std::vector<Perceptron>> brain;

  // Function to initialize the layers in NN
  void initializeLayers();

  // Function to calculate Hyper Tangent
  double hyperTan(double);

  // Function to calculate softmax
  std::vector<double> softmax(std::vector<Perceptron>);

  // Function to generate a vector of outputs from the vector of
  // input percptrons
  std::vector<double> generateVector(std::vector<Perceptron>);
};
